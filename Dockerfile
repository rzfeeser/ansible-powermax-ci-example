# we want to start with this working image
#    your starting image may come from
#    GitLab project, Docker Hub (default unless otherwise specified), Artifactory, or another "image repository"
#    it is also possible to start with an empty "scratch" image


FROM hub.docker.com/python:3.6


# if we needed to update any linux tools, we can use apt package installer
# RUN apt-get update


# create a working directory inside of the container
WORKDIR /usr/src/app


# the file requirements.txt is expected to be in our repository
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt


# the PowerMax collection requires an SDK in order to execute
RUN pip install PyU4V>=9.1.2.0


# install the powermax collection
# install through galaxy.ansible.com
RUN ansible-galaxy collection install dellemc.powermax
# install INSTEAD from a repository
# RUN ansible-galaxy collection install git+https://github.com/dell/ansible-powermax


# copy the playbook (work to do) into the container
COPY playbook-powermax.yml /usr/src/app


# Run our Ansible playbook
CMD ["ansible-playbook", "playbook-powermax.yml"]
