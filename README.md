# PowerMax Example

Putting together all of the necessary files to drive a PowerMax solution through GitLab using GitLab CI.

## Things to Check out
- Repo:
  - `.gitlab-ci.yml` - This is the automation file that tells GitLab what to do upon triggering a CI event.
  - `playbook-powermax.yml` - This is the Ansible playbook that we want to run.
  - `Dockerfile` - Describes how to build a container (runtime env) for our Ansible playbook.
  - `requirements.txt` - Python requirements